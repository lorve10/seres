$(document).ready(function(){
    buscarCategoria(); 
    
    function buscarCategoria(){
            $.ajax({
                url:'../controlador/categoria.read.php',
                type: 'POST',
                dataType: 'json',
                data : null,
            }).done(function(json){
                var categorias = '';
                        $.each(json, function(key, value){
                            // categrorias += '<tr>';
                            //     categrorias += '<td><img class="img-thumbnail img-fluid" style="float: left;" src="../'+value.img_categoria+'"></img></td>'; 
                            //     categrorias += '<td><div><b>'+value.categoria+'</b></div>';
                            //     categrorias += '<td><a class="btn btn-success">Ver Más</a></td>';
                            // categrorias += '</tr>';
                            categorias += "<div class='col-md-2 col-xs-12 col-sm-12 mt-3' >";
                            categorias += '<p><strong>'+value.categoria+'</strong></p>';
                            categorias += '<img src="../componente/img/categorias/'+value.img_categoria    +'" title="'+value.categoria+'" class="img-thumbnail img-fluid imgCate">';
                            categorias += '<p>Contamos con</p>';
                            categorias += '<a href="" class="btn btn-primary btnr">Buscar Oferta</a>';
                            categorias += "</div>";
                        })
                        categorias += '<div>';
                        categorias += '</div>';
                          
                //     categrorias += '</tbody>';
                // categrorias += '</table>';
                $('#categorias').html(categorias);
                $('#tablaCLie').DataTable({
                    "destroy" : true,
                    "autoWidth": false,
                    "searching": false,
                    "info":     false,
                    "ordering": false,
                    "lengthMenu":	[[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
                    "iDisplayLength":	5,
                    "language": {"url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",                  
                                },
                });
                $('#txtcategorias').html(categorias);
            }).fail(function(xhr, status, error){
                $('#txtcategorias').html(error);
        })
    }

})