<?php
namespace entidad;

class Comentario {

    public $idOferta;
    public $usuarioComentario;
    public $comentario;
    public $calificacion;


    /**
     * Get the value of idOferta
     */ 
    public function getIdOferta()
    {
        return $this->idOferta;
    }

    /**
     * Set the value of idOferta
     *
     * @return  self
     */ 
    public function setIdOferta($idOferta)
    {
        $this->idOferta = $idOferta;

        return $this;
    }

    /**
     * Get the value of usuarioComentario
     */ 
    public function getUsuarioComentario()
    {
        return $this->usuarioComentario;
    }

    /**
     * Set the value of usuarioComentario
     *
     * @return  self
     */ 
    public function setUsuarioComentario($usuarioComentario)
    {
        $this->usuarioComentario = $usuarioComentario;

        return $this;
    }

    /**
     * Get the value of comentario
     */ 
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set the value of comentario
     *
     * @return  self
     */ 
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get the value of calificacion
     */ 
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set the value of calificacion
     *
     * @return  self
     */ 
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }
}

