<?php

include_once "../entidad/misOfertas.entidad.php";
include_once "../modelo/misOfertas.modelo.php";

$idOfertaMod = $_POST['numIdOferta'];
$idCategoriaMod = $_POST['txtIdCategoria'];
// $nombreCatMod = $_POST['txtIdNomCategoria'];
$idSubCategoriaMod = $_POST['txtIdSubCategoria'];
// $nombreSubCatMod = $_POST['txtIdNomSubcategoria'];
$descripcionMod = $_POST['txtIdDescripcion'];
$precioOfeMod = $_POST['txtIdPrecioOfeMod'];
$UnidadMedidaMod = $_POST['txtUniMedi'];


  

$misOfertasE = new \entidad\MisOfertas();
$misOfertasE->setId_Oferta($idOfertaMod);
$misOfertasE->setIdCategoria($idCategoriaMod);
// $misOfertasE->setCategoria($nombreCatMod);
$misOfertasE->setIdSubCategoria($idSubCategoriaMod);
// $misOfertasE->setSubCategoria($nombreSubCatMod);
$misOfertasE->setDescripcion($descripcionMod);
$misOfertasE->setPrecioOfer($precioOfeMod);
$misOfertasE->setUnidadMedida($UnidadMedidaMod);


$misOfertasM = new \modelo\MisOfertas($misOfertasE);
$resultado = $misOfertasM->update();

unset($misOfertasE);
unset($misOfertasM);

echo json_encode($resultado);

?>