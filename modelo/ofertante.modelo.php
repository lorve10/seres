<?php
namespace modelo;

include_once("../entidad/ofertante.entidad.php");
include_once("../entorno/conexion.php");

use FFI\Exception;
use PDO;

class ofertante
{
    public $idOfertante;
    public $persona;
    public $descripcion;

    public  $result;
    public  $retorno;
    public  $conexion;
    public  $sql;

    public function __construct(\entidad\ofertante $ofertante){

        $this->idOfertante=$ofertante->getIdOfertante();
        $this->persona=$ofertante->getPersona();
        $this->descripcion=$ofertante->getDescripcion();
        $this-> conexion = new \conexion();

    }

    public function update(){
    
            try {
                $this->sql="UPDATE ofertante SET descripcion_ofertante='$this->descripcion'
                WHERE id_ofertante='$this->idOfertante' ";
                $this->result=$this->conexion->conn->query($this->sql);
                $this->retorno="actualizado";
    
            } catch (Exception $e) {
                $this->retorno =$e->getMessage();
            }
            return $this->retorno;
        
    }



}

?>