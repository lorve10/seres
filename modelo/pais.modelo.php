<?php
namespace modelo;
use PDO;
use Exception;

include_once '../entidad/pais.entidad.php';
include_once '../entorno/conexion.php';

class Pais{

public $id_pais;
public $pais;
public $codigo_dane;
public $estado;


 // OTROS ATRIBUTOS //
 public $conexion;
 private $result;
 private $retorno;
 private $sql;

public function __construct(\entidad\Pais $paisE){

  $this->id_pais = $paisE-> getId_pais();
  $this->pais = $paisE->getPais();
  $this->codigo_dane =$paisE->getCodigo_dane();
  $this->estado = $paisE->getEstado();
 

     $this->conexion = new \Conexion();
}



public function read()
{

  try {
       $this->sql = "SELECT * FROM pais";
       $this->result = $this->conexion->conn->query($this->sql);
       $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);
            
  } catch (Exception $e) {
       $this->retorno = $e->getMessage();
  }
       return $this->retorno;
  }

  





    }


  ?>