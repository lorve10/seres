<?php

namespace modelo;
use PDO;
use Exception;

include_once '../entidad/subcategoria.entidad.php';
include_once '../entorno/conexion.php';

class Subcategoria{
    
     public $id_subcategoria;
     public $subcategoria;
     public $id_categoria;
     
    // OTROS ATRIBUTOS //
    public $conexion;
    private $result;
    private $retorno;
    private $sql;

   public function __construct(\entidad\Subcategoria $subcategoriaE)
   {
        $this->id_subcategoria = $subcategoriaE->getId_subcategoria();
        $this->subcategoria = $subcategoriaE->getSubcategoria();
        $this->id_categoria = $subcategoriaE->getid_categoria();
        $this->conexion = new \Conexion ();
   }

   public function autocomplete()
   {

     try {
          $this->sql = "SELECT * 
          
          FROM subcategoria
          
          WHERE subcategoria LIKE CONCAT('%',$this->subcategoria,'%') AND estado='A'";
          $this->result = $this->conexion->conn->query($this->sql);
          $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);

          foreach ($this->retorno as $key => $value) {
               $this->informacion[] = array(
                    "id_subcategoria" => $value['id_subcategoria'],
                    "subcategoria" =>  $value['subcategoria'],
                    "label" => $value['subcategoria'],
                    "id_categoria" =>  $value['id_categoria']);

          }
     } catch (Exception $e) {
          $this->informacion = $e->getMessage();
     }
          return $this->informacion;
     }

     public function read()
     {
  
       try {
            $this->sql = "SELECT 
               subcategoria.`subcategoria`,
               categoria.`categoria`

               FROM 
                    subcategoria 
                    
               INNER JOIN categoria ON subcategoria.`categoria`= categoria.`id_categoria`
               
               WHERE subcategoria.estado='A'
               ORDER BY categoria.`id_categoria` ASC";
            $this->result = $this->conexion->conn->query($this->sql);
            $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);
                 
       } catch (Exception $e) {
            $this->retorno = $e->getMessage();
       }
            return $this->retorno;
       }

       public function readadm()
       {
    
         try {
              $this->sql = "SELECT 
                 subcategoria.`estado`,
                 subcategoria.`subcategoria`,
                 categoria.`categoria`
  
                 FROM 
                      subcategoria 
                      
                 INNER JOIN categoria ON subcategoria.`categoria`= categoria.`id_categoria`
               
                 ORDER BY categoria.`id_categoria` ASC";
              $this->result = $this->conexion->conn->query($this->sql);
              $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);
                   
         } catch (Exception $e) {
              $this->retorno = $e->getMessage();
         }
              return $this->retorno;
         }
}

?>