<?php

namespace modelo;
use PDO;
use Exception;

include_once '../entidad/categoria.entidad.php';
include_once '../entorno/conexionSingleton.php';



class Categoria{
    
    public $id_categoria;
    public $categria;
    public $img_categoria;


    // OTROS ATRIBUTOS //
    public $conexion;
    private $result;
    private $retorno;
    private $sql;

   public function __construct(\entidad\Categoria $categoriaE)
   {
        $this->id_categoria = $categoriaE->getId_categoria();
        $this->categoria = $categoriaE->getCategoria();
        $this->img_categoria = $categoriaE->getImg_categoria();
        $this->conexion = \Conexion::singleton();
   }

  
   public function read()
   {

     try {
          $this->sql = "SELECT * FROM categoria WHERE estado = 'A'";
          $this->result = $this->conexion->query($this->sql);
          $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);
               
     } catch (Exception $e) {
          $this->retorno = $e->getMessage();
     }
          return $this->retorno;
     }

   public function readadm()
   {

     try {
          $this->sql = "SELECT * FROM categoria ";
          $this->result = $this->conexion->query($this->sql);
          $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);
               
     } catch (Exception $e) {
          $this->retorno = $e->getMessage();
     }
          return $this->retorno;
     }

}

?>