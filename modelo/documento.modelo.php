<?php

namespace modelo;
use PDO;
use Exception;

include_once '../entidad/documento.entidad.php';
include_once '../entorno/conexion.php';

class Documento{
    
     
    public $id_documento;
    public $nombre;
    public $nomenclatura;
    public $estado;

    // OTROS ATRIBUTOS //
    public $conexion;
    private $result;
    private $retorno;
    private $sql;

   public function __construct(\entidad\Documento $documetoE){

        $this->conexion = new \Conexion();
   }

  
   public function read()
   {

     try {
          $this->sql = "SELECT * FROM tipo_documento ";
          $this->result = $this->conexion->conn->query($this->sql);
          $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);
               
     } catch (Exception $e) {
          $this->retorno = $e->getMessage();
     }
          return $this->retorno;
     }

    
    

}

?>