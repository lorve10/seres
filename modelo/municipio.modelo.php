<?php
namespace modelo;
use PDO;
use Exception;

include_once '../entidad/municipio.entidad.php';
include_once '../entorno/conexion.php';

class Municipio{

    public  $id_municipio;
    public  $municipio;
    public  $codigo_dane;
    public  $id_departamento;
    public  $estado;
    

 // OTROS ATRIBUTOS //
 public $conexion;
 private $result;
 private $retorno;
 private $sql;

public function __construct(\entidad\Municipio $municipioE){

  $this->id_municipio = $municipioE-> getId_municipio();
  $this->municipio = $municipioE-> getMunicipio();
  $this->codigo_dane =$municipioE->getCodigo_dane();
  $this->id_departamento =$municipioE->getId_departamento();
  $this->estado =$municipioE->getEstado();
  
 

     $this->conexion = new \Conexion();
}



public function read()
{

  try {
       $this->sql = "SELECT * FROM municipio";
       $this->result = $this->conexion->conn->query($this->sql);
       $this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);
            
  } catch (Exception $e) {
       $this->retorno = $e->getMessage();
  }
       return $this->retorno;
  }

  



 }
  ?>