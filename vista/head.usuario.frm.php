<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../componente/css/globales/bootstrap.css">
    <link rel="stylesheet" href="../componente/css/globales/all.css">  
    <link rel="stylesheet" href="../componente/css/globales/datatables.css">
    <link rel="stylesheet" href="../componente/css/globales/jquery-ui.css"> 
    <link rel="stylesheet" href="../componente/css/globales/global.css"> 
    <link rel="stylesheet" href="../componente/css/globales/alertify.css">
    <link rel="stylesheet" href="../componente/css/globales/style.css">    
    <link rel="stylesheet" href="../componente/css/globales/calificacion.css">  
    <link rel="stylesheet" href="../componente/css/globales/fontawesome.css">
    <link rel="stylesheet" href="../componente/css/globales/mi_perfil.css"> 
    <link rel="stylesheet" href="../componente/css/globales/regular.css"> 
    <link rel="stylesheet" href="../componente/css/globales/solid.css"> 
    <link rel="stylesheet" href="../componente/css/globales/svg-with-js.css"> 


    <script src="../componente/libreria/jquery-3.5.1.js" ></script>   
    <script src="../componente/libreria/bootstrap.js"></script> 
    <script src="../componente/libreria/all.js"></script>
    <script src="../componente/libreria/datatables.js"></script>
    <script src="../componente/libreria/dataTables.buttons.js"></script>
    <script src="../componente/libreria/pdfmake.min.js"></script>
    <script src="../componente/libreria/regular.js"></script>
    <script src="../componente/libreria/vfs_fonts.js"></script>
    <script src="../componente/libreria/buttons.html5.min.js"></script>
    <script src="../componente/libreria/buttons.print.min.js"></script>
    <script src="../componente/libreria/jszip.min.js"></script>
    <script src="../componente/libreria/jquery-ui.js"></script> 
    <script src="../componente/libreria/alertify.js"></script>  
    <script src="../componente/libreria/fontawesome.js"></script>       
    <script src="../js/globales/global.js"></script>
    
    <div  class="container-fluid">
    <div class="">
        <div class="topnav" id="myTopnav"> 
            <a class="navbar-brand" href="mi_perfil.frm.php"><img src="../componente/img/globales/Logo_SERES.png" style="width: 100px;"></a>
            <a class="nav-link" href="buscar.oferta.usuario.frm.php"><i class="fas fa-search"></i> Buscar Oferta</a>
            <a class="nav-link" href="publicar.oferta.php"><i class="fas fa-plus-circle"></i> Publicar Oferta</a>
            <a class="nav-link" href="misOfertas.frm.php"><i class="far fa-address-book"></i> Mis Ofertas</a>
            <a class="nav-link" href="mi_perfil.frm.php"><i class="fas fa-user-alt"></i> Mi Perfil</a>
            <a href="javascript:void(0);" class="icon" onclick="Menu()"><i class="fa fa-bars"></i></a>
            <a  class="nav-link"  id="btnCerrar"><i class="fas fa-sign-out-alt"></i> Cerrar Sesion</a>
            <script src="../js/login/logueo.js"></script>

            
        </div>  
        
        </div>
    </div>

