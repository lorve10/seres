<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../componente/css/globales/bootstrap.css">
    <link rel="stylesheet" href="../componente/css/globales/all.css">  
    <link rel="stylesheet" href="../componente/css/globales/datatables.css">
    <link rel="stylesheet" href="../componente/css/globales/jquery-ui.css"> 
    <link rel="stylesheet" href="../componente/css/globales/global.css"> 
    <link rel="stylesheet" href="../componente/css/globales/alertify.css">
    <link rel="stylesheet" href="../componente/css/globales/style.css">    
    <link rel="stylesheet" href="../componente/css/globales/calificacion.css">  
    <link rel="stylesheet" href="../componente/css/globales/fontawesome.css">
    <link rel="stylesheet" href="../componente/css/globales/mi_perfil.css"> 
    <link rel="stylesheet" href="../componente/css/globales/regular.css"> 
    <link rel="stylesheet" href="../componente/css/globales/solid.css"> 
    <link rel="stylesheet" href="../componente/css/globales/svg-with-js.css"> 

    <script src="../componente/libreria/jquery-3.5.1.js" ></script>   
    <script src="../componente/libreria/bootstrap.js"></script> 
    <script src="../componente/libreria/all.js"></script>
    <script src="../componente/libreria/datatables.js"></script>
    <script src="../componente/libreria/dataTables.buttons.js"></script>
    <script src="../componente/libreria/pdfmake.min.js"></script>
    <script src="../componente/libreria/regular.js"></script>
    <script src="../componente/libreria/vfs_fonts.js"></script>
    <script src="../componente/libreria/buttons.html5.min.js"></script>
    <script src="../componente/libreria/buttons.print.min.js"></script>
    <script src="../componente/libreria/jszip.min.js"></script>
    <script src="../componente/libreria/jquery-ui.js"></script> 
    <script src="../componente/libreria/alertify.js"></script>  
    <script src="../componente/libreria/fontawesome.js"></script>       
    <script src="../js/globales/global.js"></script>
    
    <title>Como Funciona</title>
</head>
<body>
    <div  class="container">
        <div class="row">
            <div class="topnav" id="myTopnav"> 
                <a class="navbar-brand" href="index.frm.php"><img src="../componente/img/globales/Logo_SERES.png"style="width: 100px;"></a>
                <a class="nav-link " href="index.frm.php">Inicio</a>
                <a href="javascript:void(0);" class="icon" onclick="Menu()"><i class="fa fa-bars"></i></a>
            </div>  
        </div>
    </div>
    <!-- /// Inicio Banner /// -->
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
      <ol class="carousel-indicators">
        <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
        <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
        <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="../componente/img/plantilla.comofunciona/1.png" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="../componente/img/plantilla.comofunciona/2.png" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="../componente/img/plantilla.comofunciona/2.png" class="d-block w-100" alt="...">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previo</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Proximo</span>
      </a>
    </div>  
    <!-- /// Fin Banner /// -->
    <!-- /// Inicio Submenú/// -->
    <div class="row " >
      <div class="col-md-12 col-xs-12 col-sm-12  border-bottom">
        <ul class="nav justify-content-center">
          <li class="nav-item">
            <a class="nav-link" href="solicitante.frm.php">Como funciona</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="preguntas.fre.frm.php">Preguntas frecuentes</a>
          </li>
        </ul>
      </div>
    </div> 
    <!-- /// Fin Submenú ///  -->  
</body>
</html>