<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../componente/css/globales/bootstrap.css">
    <link rel="stylesheet" href="../componente/css/globales/all.css">
    <link rel="stylesheet" href="../componente/css/globales/datatables.css">
    <link rel="stylesheet" href="../componente/css/globales/jquery-ui.css">
    <link rel="stylesheet" href="../componente/css/globales/global.css">
    <link rel="stylesheet" href="../componente/css/globales/alertify.css">
    <link rel="stylesheet" href="../componente/css/globales/style.css">
    <link rel="stylesheet" href="../componente/css/globales/calificacion.css">
    <link rel="stylesheet" href="../componente/css/globales/fontawesome.css">
    <link rel="stylesheet" href="../componente/css/globales/mi_perfil.css">
    <link rel="stylesheet" href="../componente/css/globales/regular.css">
    <link rel="stylesheet" href="../componente/css/globales/solid.css">
    <link rel="stylesheet" href="../componente/css/globales/svg-with-js.css">

    <script src="../componente/libreria/jquery-3.5.1.js" ></script>   
    <script src="../componente/libreria/bootstrap.js"></script> 
    <script src="../componente/libreria/all.js"></script>
    <script src="../componente/libreria/datatables.js"></script>
    <script src="../componente/libreria/dataTables.buttons.js"></script>
    <script src="../componente/libreria/pdfmake.min.js"></script>
    <script src="../componente/libreria/regular.js"></script>
    <script src="../componente/libreria/vfs_fonts.js"></script>
    <script src="../componente/libreria/buttons.html5.min.js"></script>
    <script src="../componente/libreria/buttons.print.min.js"></script>
    <script src="../componente/libreria/jszip.min.js"></script>
    <script src="../componente/libreria/jquery-ui.js"></script> 
    <script src="../componente/libreria/alertify.js"></script>  
    <script src="../componente/libreria/fontawesome.js"></script>       
    <script src="../js/globales/global.js"></script>
    
    <div  class="container-fluid">
    <div class="" id="top">
        <div class="topnav" id="myTopnav"> 
            <a class="navbar-brand" href="index.frm.php"><img src="../componente/img/globales/Logo_SERES.png"style="width: 100px;"></a>
            <a class="nav-link " href="index.frm.php">Inicio</a>
            <a class="nav-link" href="#foot">Acerca de Nosotros</a>
            <!-- <a class="nav-link" href="solicitante.frm.php">Cómo Funciona</a> -->
            <a class="nav-link" href="buscar.oferta.frm.php">Buscar Oferta</a>
            <a class="nav-link" href="registro.frm.php">Registrarse</a>
            <a class="nav-link" data-bs-toggle="modal" data-bs-target="#Modaliniciarsesion"> Iniciar sesión </a>
            <a href="javascript:void(0);" class="icon" onclick="Menu()"><i class="fa fa-bars"></i></a>
        </div>  
        </div>
    </div>
    <!-- Modal  de iniciar sesion-->

    <form id="LoginF">
        <div class="modal fade" id="Modaliniciarsesion" tabindex="-1" aria-labelledby="Modaliniciar" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="Modaliniciar">Iniciar Sesion</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <!--   se colocan los datos  que hay que llenar   -->
                        <label for="formFile" class="form-label">Ingrese su Email</label>
                        <input class="form-control form-control-lg" type="text" placeholder="Email" id="txtUsuario"
                            name="txtUsuario" aria-label=".form-control-lg example">
                        <label for="formFile" class="form-label">Ingrese su Contraseña</label>
                        <input class="form-control form-control-lg" type="password" placeholder="Contraseña"
                            id="txtClave" name="txtClave" aria-label=".form-control-lg example">
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <button class="btn btn-success  btn-xl m-2 p-2" type="button" id="btnLogin">Iniciar
                                Sesion</button>
                            <a href="#" class="m-1 p-1" style="text-decoration:none">¿Olvido su
                                contraseña?</a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="registro.frm.php" class="m-1 p-1" style="text-decoration:none">Registrase</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="../js/login/login.js"></script>
    </div>