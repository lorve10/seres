
<?php include_once "head.usuario.frm.php" ?>


<?php
include_once("../controlador/logueo.read.php");
    if (!isset($_SESSION['id_rol'])) {
        header("location: login.php");
    } else {
        if ($_SESSION['id_rol']!=2) {
            header('location: publicar.oferta.php');
        }
    }
    
?>

<title>Mis Ofertas</title>
</head>
<body>

<div class="container border" >

<form class="form-group" id="misOfertasFrm" action="" >
        <div class="border-bottom text-center">
            <h2>Mis Ofertas</h2>

            <input type="text" id="idOfertante" value="<?php $idofertante= $_SESSION['id_ofertante'];  echo $idofertante; ?>" hidden> 
            <br>
        </div>

        <div  id="misOfertas"></div>      
        
        <!-- /// INICIO MODAL COMENTARIOS -->
        <div class="modal fade"  id="modalVerMAs" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><strong>Comentarios de la oferta</strong> </h5>
                            <button type="button" value="" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <!-- <span aria-hidden="true">&times;</span> -->                                
                            </button>
                        </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12 ">
                                <input class="form-control" type="number" name="numIdVerMas" id="numIdVerMas" hidden >
                            </div>
                        </div>

                        <!-- <div class="row">
                                <div class="col-12 text-center">
                                    <button type="button" id="btnLeerComen" class="btn btn-primary" data-dismiss="modal">Leer comentarios</button>
                                </div>
                        </div> -->

                        <div class=" col-md-12  " id="bodyModalComentarios"></div>                           

                    </div>
                    <div class="modal-footer">                            
                        <button type="button" onclick="limpiar()" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    </div>
                    </div>
                </div>
        </div>
        <!-- /// FIN MODAL COMENTARIOS -->
       
        <!-- /// INICIO MODAL MODIFICAR -->
        <div class="modal"  id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><strong>Modificar Oferta</strong> </h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <!-- <span aria-hidden="true">&times;</span> -->
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                        <div class="col-12">
                                            <input class="form-control" type="text" name="numIdOferta" id="numIdOferta" >
                                        </div>
                                </div>                              
                                <div class="row">   
                                        <div class="col-12">                                            
                                            <label><strong>Categoria</strong> </label> <br>
                                            <label type="text" name="opCategoria1" id="opCategoria1"></label>
                                            <select name="txtIdCategoria" id="txtIdCategoria"> </select>                           
                                        </div>
                                </div>
                                <h3></h3>
                                <div class="row">
                                        <div class="col-12">
                                            <input class="form-control" type="number" name="inIdCategoria" id="inIdCategoria" >
                                            <label><strong>Subcategoria</strong> </label><br>
                                            <select name="txtIdSubCategoria" id="txtIdSubCategoria" > 
                                                <!-- <option selected>- Seleccione una subcategoría -</option>                                              -->
                                            </select>
                                        </div>
                                </div>
                                <h3></h3>   
                                                            
                                <div class="row">
                                        <div class="col-12 text-left">
                                            <label><strong>Descripción</strong> </label>                                            
                                        </div>
                                        <div class="col-12 text-center">
                                            <textarea name="txtIdDescripcion" id="txtIdDescripcion" cols="55" rows="5" ></textarea>
                                        </div>
                                </div>                                
                                <h3></h3>
                                <div class="row">
                                        <div class="col-12">
                                            <label for=""><strong>Valor</strong> </label>
                                            <input class="form-control" type="number" name="txtIdPrecioOfeMod" id="txtIdPrecioOfeMod">
                                        </div>
                                </div>
                                <h3></h3>
                                <div class="row">
                                        <div class="col-12">
                                            <label for=""><strong>Unidad de medida</strong> </label><br>
                                            <select name="txtUniMedi" id="txtUniMedi"></select>
                                        </div>
                                </div>
                            </div>
                            <div class="modal-footer">                            
                                <button type="button" id="btnModificar" class="btn btn-primary" data-bs-dismiss="modal">Guardar</button>
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                            </div>
                            </div>
                        </div>
        </div>
        <!-- /// FIN MODAL MODIFICAR -->

        <!-- MODAL ELIMINAR -->
        <div class="modal"  id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Eliminar oferta</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                            <!-- <span aria-hidden="true">&times;</span> -->
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                    <div class="col-12">
                                        <input type="number" name="txtIdOferElm" id="txtIdOferElm" hidden >
                                        <h5>¿Está seguro que desea eliminar la oferta?</h5>
                                        <h4 id="oferEliminar"></h4>
                                    </div>
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" id="btnEliminar" class="btn btn-danger" data-bs-dismiss="modal">Eliminar</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
        </div>
        <!-- FIN MODAL ELIMINAR -->
    
    </form>
</div>

<br>
<!-- <div class="text-center">
    <a href="mi_perfil.frm.php" class="btn btn-primary">Atrás</a>
</div>       -->
<br>

<?php include_once "footer.frm.php" ?>
</body>
<link rel="stylesheet" href="../componente/css/globales/misOfertas/misOfertas.css"> 
<script src="../js/misOfertas.js"></script>
</html>

<!-- viviana -->