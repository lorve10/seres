<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../componente/css/globales/bootstrap.css">
    <link rel="stylesheet" href="../componente/css/globales/all.css">  
    <link rel="stylesheet" href="../componente/css/globales/datatables.css">
    <link rel="stylesheet" href="../componente/css/globales/jquery-ui.css"> 
    <link rel="stylesheet" href="../componente/css/globales/global.css"> 
    <link rel="stylesheet" href="../componente/css/globales/alertify.css">
    <link rel="stylesheet" href="../componente/css/globales/style.css">    
    <link rel="stylesheet" href="../componente/css/globales/calificacion.css">  
    <link rel="stylesheet" href="../componente/css/globales/fontawesome.css">
    <link rel="stylesheet" href="../componente/css/globales/mi_perfil.css"> 
    <link rel="stylesheet" href="../componente/css/globales/regular.css"> 
    <link rel="stylesheet" href="../componente/css/globales/solid.css"> 
    <link rel="stylesheet" href="../componente/css/globales/svg-with-js.css"> 

    <script src="../componente/libreria/jquery-3.5.1.js" ></script>   
    <script src="../componente/libreria/bootstrap.js"></script> 
    <script src="../componente/libreria/all.js"></script>
    <script src="../componente/libreria/datatables.js"></script>
    <script src="../componente/libreria/dataTables.buttons.js"></script>
    <script src="../componente/libreria/pdfmake.min.js"></script>
    <script src="../componente/libreria/regular.js"></script>
    <script src="../componente/libreria/vfs_fonts.js"></script>
    <script src="../componente/libreria/buttons.html5.min.js"></script>
    <script src="../componente/libreria/buttons.print.min.js"></script>
    <script src="../componente/libreria/jszip.min.js"></script>
    <script src="../componente/libreria/jquery-ui.js"></script> 
    <script src="../componente/libreria/alertify.js"></script>  
    <script src="../componente/libreria/fontawesome.js"></script>       
    <script src="../js/globales/global.js"></script>
    
    <title>Contactanos</title>
</head>
<body>
    <div  class="container">
        <div class="row">
            <div class="topnav" id="myTopnav"> 
                <a class="navbar-brand" href="index.frm.php"><img src="../componente/img/globales/Logo_SERES.png"style="width: 100px;"></a>
                <a class="nav-link " href="index.frm.php">Inicio</a>
                <a href="javascript:void(0);" class="icon" onclick="Menu()"><i class="fa fa-bars"></i></a>
            </div>  
        </div>
    </div>

<div class="container border"  style="color: rgb(207, 199, 199); background-color: rgb(202, 197, 197);">
	<div class="aling-items-center justify-content-center mt-5">
		<h3 class="text-center" style="color: black;">Contactanos</h3><br>
		<img src="../componente/img/globales/Logo_SERES.png" style="width: 250px;" class="rounded-circle mx-auto d-block">
	</div>
	<div class="container col-md-6 col-xs-3 col-sm-3 ">
		<div class="mt-3 ">
			<input class="form-control" type="text" name="" id="" placeholder="Nombre">
		</div>
		<div class="mt-3 ">
			<input class="form-control" type="text" name="" id="" placeholder="Email">
		</div>
		<div class="mt-3 ">
			<input class="form-control" type="text" style="height: 180px;" name="" id="" placeholder="Descripcion">
		</div>
	</div>
	<br>
	<div class="text-center">
		<a href="principal.sesion.php" class="btn btn-success" style="width: 250px;">Enviar</a>
	</div>
    <br>
</div>
<!-- Incluye Menu -->
<?php include_once "footer.frm.php" ?>
<!-- Fin Incluye Menu -->
</body>
</html>