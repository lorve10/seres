<?php include_once "head.adm.frm.php" ?>

<title>Ofertas </title>
</head>

<body >
    <div class="container">
         <!-- Nombre formulario -->

        <div class="row bg-light  justify-content-center " >
            <form class="form-group" id="categoria">
                <div class="row justify-content-center text-center">
                    <h3 class=" text-center ">SUBCATEGORIAS</h3>
                </div>
                <br>

                 <!-- Datos para ingresar valores del formulario -->
                 <div class="row justify-content-center text-center">
                    <h5 class=" text-center ">Crear una Subcategoria</h5>
                </div>
                <div class="row ">
                    <div class="col-md-5 col-xs-6 col-sm-6 ">
                        <label> Nombre Subcategoria</label>
                        <input class="form-control" type="text" name="txtNombreCat" id="txtNombreCat">
                    </div>
                    <div class="col-md-1 col-xs-12 col-sm-12 ">
                       
                    </div>
                    <div class="col-md-5 col-xs-6 col-sm-6">
                        <label>Categoria </label>
                        <input class="form-control" type="text" name="txtPrecioCat" id="txtPrecioCat">
                    </div>
                </div>
                <br>

                 <!-- botones para realizar operaciones -->

                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-6">
                        <a id="btnRegistrar" class="btn btn-primary" >Registrar <i class="fas fa-pencil-alt"></i></a>
                    </div>
                </div>

                 <!-- datos del js -->
                 <div class="row justify-content-center text-center">
                    <h5 class=" text-center ">Subcategorias Registradas</h5>
                </div>
                <div class="row my-1 ">
                    <div class="col-md-12 col-xs-12 col-sm-6">
                        <h5 id="datos"></h5>   
                    </div>
                  
                </div>
                <div class="row my-1 col-md-12 col-xs-12 col-sm-6">
                    <h5 id="respuesta"></h5>
                </div>


            <!-- Modal Modificar -->

            <div class="modal fade" id="modalModificar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                            <div class="col-12">
                                <input type="number" name="txtIdSubCatMod" id="txtIdSubCatMod" >
                                <label> Nombre Subcategoria</label>
                                <input class="form-control" type="text" name="txtSubNombreCatMod" id="txtSubNombreCatMod">
                            </div>
                            <div class="col-12">
                                <label for="">Categoria </label>
                                <input class="form-control" type="text" name="txtCatModSub" id="txtCatModSub">
                            </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnModificar" class="btn btn-primary" data-bs-dismiss="modal">Guardar</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        </div>
                      </div>
                    </div>
                  </div>

            <!-- Fin Modal Modificar -->

            <!-- Modal eLIMINAR -->
                <div class="modal"  id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Eliminar Categoria</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                    <div class="col-12">
                                        <input type="number" name="txtIdCatElm" id="txtIdCatElm" hidden >
                                        <h5>Esta seguro de eliminar la categoria:</h5>
                                        <h4 id="catEliminar"></h4>
                                    </div>
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" id="btnEliminar" class="btn btn-danger" data-dismiss="modal">Eliminar</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

            <!-- Fin Modal Modificar -->
            </form>
        </div>
    </div> 
    <?php include_once "footer.resum.frm.php" ?>
</body>
    <script src="../../js/administrador/subcategorias.js"></script>
</html>