<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../componente/css/globales/bootstrap.css">
    <link rel="stylesheet" href="../../componente/css/globales/all.css">
    <link rel="stylesheet" href="../../componente/css/globales/datatables.css">
    <link rel="stylesheet" href="../../componente/css/globales/jquery-ui.css">
    <link rel="stylesheet" href="../../componente/css/globales/global.css">
    <link rel="stylesheet" href="../../componente/css/globales/alertify.css">
    <link rel="stylesheet" href="../../componente/css/globales/style.css">
    <link rel="stylesheet" href="../../componente/css/globales/calificacion.css">
    <link rel="stylesheet" href="../../componente/css/globales/fontawesome.css">
    <link rel="stylesheet" href="../../componente/css/globales/mi_perfil.css">
    <link rel="stylesheet" href="../../componente/css/globales/regular.css">
    <link rel="stylesheet" href="../../componente/css/globales/solid.css">
    <link rel="stylesheet" href="../../componente/css/globales/svg-with-js.css">

    <script src="../../componente/libreria/jquery-3.5.1.js" ></script>   
    <script src="../../componente/libreria/bootstrap.js"></script> 
    <script src="../../componente/libreria/all.js"></script>
    <script src="../../componente/libreria/datatables.js"></script>
    <script src="../../componente/libreria/dataTables.buttons.js"></script>
    <script src="../../componente/libreria/pdfmake.min.js"></script>
    <script src="../../componente/libreria/regular.js"></script>
    <script src="../../componente/libreria/vfs_fonts.js"></script>
    <script src="../../componente/libreria/buttons.html5.min.js"></script>
    <script src="../../componente/libreria/buttons.print.min.js"></script>
    <script src="../../componente/libreria/jszip.min.js"></script>
    <script src="../../componente/libreria/jquery-ui.js"></script>
    <script src="../../componente/libreria/alertify.js"></script>
    <script src="../../componente/libreria/fontawesome.js"></script>
    <script src="../../js/globales/global.js"></script>
    
    <title>Iniciar Sesion Administrador</title>
</head>
<body>
    <div  class="container">
        <div class="row">
            <div class="topnav" id="myTopnav"> 
                <a class="navbar-brand" href=""><img src="../../componente/img/globales/Logo_SERES.png"style="width: 100px;"></a>
                <!-- <a class="nav-link " href="index.frm.php">Inicio</a> -->
                <a href="javascript:void(0);" class="icon" onclick="Menu()"><i class="fa fa-bars"></i></a>
            </div>  
        </div>
        <div  class="row justify-content-center">
            <div class=" border col-md-6 col-xs-12 col-sm-12 m-2 p-2">
                <form id="LoginF">
                    <label for="formFile" class="form-label">Ingrese su Email</label>
                    <input class="form-control form-control-lg" type="text" placeholder="Email" id="txtUsuario" name="txtUsuario"
                    aria-label=".form-control-lg example">
                    <label for="formFile" class="form-label">Ingrese su Contraseña</label>
                    <input class="form-control form-control-lg" type="password" placeholder="Contraseña" id="txtClave" name="txtClave"
                    aria-label=".form-control-lg example">
                    <div class="d-grid gap-2 col-12 mx-auto">
                        <button class="btn btn-success  btn-xl m-2 p-2" type="button" id="btnLogin" >Iniciar Sesion</button>
                    </div>
                    <div class="col-6 mx-auto ">
                        <a href="#" class="m-1 p-1" style="text-decoration:none">¿Olvido su contraseña?</a>
                    </div>
                    <div class="col-6 mx-auto ">
                        <a href="registro.frm.php" class="m-1 p-1" style="text-decoration:none">Registrarse</a>
                    </div>
                </form>
            </div>  
        </div>
    </div>
    <br>


     <?php include_once "footer.resum.frm.php" ?>
</body>
<script src="../js/login/login.js"></script>
</html>
