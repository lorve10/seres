<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../componente/css/globales/bootstrap.css">
    <link rel="stylesheet" href="../../componente/css/globales/all.css">
    <link rel="stylesheet" href="../../componente/css/globales/datatables.css">
    <link rel="stylesheet" href="../../componente/css/globales/jquery-ui.css">
    <link rel="stylesheet" href="../../componente/css/globales/global.css">
    <link rel="stylesheet" href="../../componente/css/globales/alertify.css">
    <link rel="stylesheet" href="../../componente/css/globales/style.css">
    <link rel="stylesheet" href="../../componente/css/globales/calificacion.css">
    <link rel="stylesheet" href="../../componente/css/globales/fontawesome.css">
    <link rel="stylesheet" href="../../componente/css/globales/mi_perfil.css">
    <link rel="stylesheet" href="../../componente/css/globales/regular.css">
    <link rel="stylesheet" href="../../componente/css/globales/solid.css">
    <link rel="stylesheet" href="../../componente/css/globales/svg-with-js.css">
    <link rel="stylesheet" href="../../componente/css/globales/autoFill.bootstrap4.css">
    <link rel="stylesheet" href="../../componente/css/globales/autoFill.dataTables.css">
    <link rel="stylesheet" href="../../componente/css/globales/autoFill.foundation.css">
    <link rel="stylesheet" href="../../componente/css/globales/buttons.bootstrap4.css">
    <link rel="stylesheet" href="../../componente/css/globales/buttons.dataTables.css">
    <link rel="stylesheet" href="../../componente/css/globales/colReorder.bootstrap4.css">
    <link rel="stylesheet" href="../../componente/css/globales/colReorder.dataTables.css">
    <link rel="stylesheet" href="../../componente/css/globales/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="../../componente/css/globales/dataTables.dateTime.css">
    <link rel="stylesheet" href="../../componente/css/globales/dataTables.jqueryui.css">
    <link rel="stylesheet" href="../../componente/css/globales/responsive.bootstrap4.css">
    <link rel="stylesheet" href="../../componente/css/globales/responsive.dataTables.css">

    <script src="../../componente/libreria/jquery-3.5.1.js" ></script>   
    <script src="../../componente/libreria/bootstrap.js"></script> 
    <script src="../../componente/libreria/all.js"></script>
    <script src="../../componente/libreria/datatables.js"></script>
    <script src="../../componente/libreria/dataTables.buttons.js"></script>
    <script src="../../componente/libreria/pdfmake.min.js"></script>
    <script src="../../componente/libreria/regular.js"></script>
    <script src="../../componente/libreria/vfs_fonts.js"></script>
    <script src="../../componente/libreria/buttons.html5.min.js"></script>
    <script src="../../componente/libreria/buttons.print.min.js"></script>
    <script src="../../componente/libreria/jszip.min.js"></script>
    <script src="../../componente/libreria/jquery-ui.js"></script> 
    <script src="../../componente/libreria/alertify.js"></script>      
    <script src="../../componente/libreria/autoFill.bootstrap4.js"></script>   
    <script src="../../componente/libreria/autoFill.foundation.js"></script>  
    <script src="../../componente/libreria/buttons.bootstrap4.js"></script>   
    <script src="../../componente/libreria/buttons.jqueryui.js"></script>   
    <script src="../../componente/libreria/buttons.print.js"></script>    
    <script src="../../componente/libreria/colReorder.bootstrap4.js"></script>   
    <script src="../../componente/libreria/colReorder.dataTables.js"></script> 
    <script src="../../componente/libreria/dataTables.bootstrap4.js"></script>   
    <script src="../../componente/libreria/dataTables.dateTime.js"></script>    
    <script src="../../componente/libreria/dataTables.fixedColumns.js"></script>   
    <script src="../../componente/libreria/dataTables.foundation.js"></script>   
    <script src="../../componente/libreria/dataTables.responsive.js"></script>   
    <script src="../../componente/libreria/fixedColumns.bootstrap4.js"></script>   
    <script src="../../componente/libreria/fixedColumns.dataTables.js"></script>   
    <script src="../../componente/libreria/responsive.bootstrap4.js"></script>   
    <script src="../../js/globales/global.js"></script>


    <div  class="container-fluid">
    <div class="">
        <div class="topnav" id="myTopnav"> 
            <a class="navbar-brand" href=""><img src="../../componente/img/globales/Logo_SERES.png"style="width: 100px;"></a>
            <a class="nav-link " href="">Inicio</a>
            <a class="nav-link" href="categoria.adm.frm.php">Categorias</a>
            <a class="nav-link" href="subcategoria.adm.frm.php">Subcategorias</a>
            <a class="nav-link" href="ofertas.adm.frm.php">Ofertas</a>
            <a class="nav-link" href="comentarios.adm.frm.php">Comentarios</a>
            <a class="nav-link" href="">Ofertantes</a>
            <a class="nav-link" href="">Paises</a>
            <a class="nav-link" href="">Departamentos</a>
            <a class="nav-link" href="">Municipios</a>
            
            <a href="javascript:void(0);" class="icon" onclick="Menu()"><i class="fa fa-bars"></i></a>
        </div>  
        </div>
    </div>
