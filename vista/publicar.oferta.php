<?php include_once "head.usuario.frm.php" ?>

<?php
include_once("../controlador/logueo.read.php");
    if (!isset($_SESSION['id_rol'])) {
        header("location: login.php");
    } else {
        if ($_SESSION['id_rol']!=2) {
            header('location: login.php');
        }
    }
    
?>
<style type="text/css">
    .file{
        position:absolute; 
        font-size:2px; 
        opacity: 0; -moz-opacity: 0; 
        filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0)

}
.bb{          
max-width: 100%;
max-height: 100%;
padding: 20%;
margin-top: auto;

}

</style>


<title>publicar oferta</title>
</head>
<body>
<br>

<div class="container border " style="margin: auto; width: 1000px; height: 900px;">

<h2 class="text-center">Publicar Oferta</h2>
<br>
    <form id="oferta" class="row g-3" action="../controlador/publicarOferta.create.php" method="POST" enctype="multipart/form-data" >
    <input type="hidden" value="<?php $idofertante= $_SESSION['id_ofertante'];  echo $idofertante; ?>" id="idofertante" name="idofertante" >
    <div class="col-md-5">
            
            <label for="categoria" class="form-label">Categoria</label>
                <select  class="form-select" aria-label="Seleccionar categoria" id="categorias" name="categorias">
                    
                </select>

            </div>
            <div class="col-md-5 ms-auto" >
            <label for="subcategoria" class="form-label">Subcategoria</label>
                <select class="form-select" aria-label="Seleccionar Subcategoria"  id="subcategorias" name="subcategorias">
                </select>
            </div>
            <br>
            <br>
            <div class="justify-content-center aling-item-center text-center mt-5">
                <label for="formFile" class="form-label">Seleccionar Imagen</label>
                <div class="justify-content-center aling-item-center text-center">
                        <div id="divImagen" class="justify-content-center aling-item-center text-center">
                            <label for="txtImagen">
                                <i class="fas fa-camera" style="width: 40%; height: 40%;" ></i>
                            </label>
                            <input type="file" name="txtImagen" id="txtImagen" class="file" >
                        </div>
                </div>
            </div>
            <br>
            <hr>
            <div class="mb-3 justify-content-center">
                <label for="">Nombre de la oferta</label>
                <input type="text" placeholder="Escribir Nombre de la oferta" class="form-control" id="txtoferta" name="txtoferta">

            </div>
            <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label" >Descripcion de la oferta</label>
            <textarea name="descripcion_oferta" class="form-control" id="descripcion_oferta" rows="3" placeholder="Descripcion del producto..."></textarea>
            </div>
            <div class="d-flex">
            <div class="col-md-4 ">
                <label for="precio" class="form-label">Precio</label>
                <input name="valor_oferta" type="number" class="form-control" id="valor_oferta" placeholder="Ingrese el precio en pesos">
            </div>
            <div class="col-md-4 ms-auto" >
                <label for="subcategoria" class="form-label">Unidad de medida</label>
                    <select class="form-select" aria-label="Seleccionar Subcategoria"  id="unidad" name="unidad">
                    </select>
                </div>
             </div>
            
            <div class="text-center">
                <button type="submit" id="btn_crear" class="btn btn-primary text-center">Publicar</button>
            </div>
            <script src="../js/publicar_oferta/publicar.oferta.js"> </script>
    </form> 
</div>

<br>
<?php include_once "footer.frm.php" ?>

</body>
</html>


<!--Melisa -->